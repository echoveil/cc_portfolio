Rails.application.routes.draw do
 
root 'pages#welcome'
get '/portfolio' => 'pages#portfolio'
get '/contact' => 'pages#contact'
end